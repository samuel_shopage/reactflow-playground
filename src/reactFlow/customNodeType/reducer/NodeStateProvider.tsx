import React from "react";
import { NodeState } from "./nodeStateStore";

const NodeStateProvider = ({ id, data, children }: any) => {
  const { cards, handles } = data;
  const initialNodeState = {
    id: id,
    cards: cards,
    handles: handles,
  };
  return <NodeState.Provider initialState={initialNodeState}> {children}</NodeState.Provider>;
};

export default NodeStateProvider;

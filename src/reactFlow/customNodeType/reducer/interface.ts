export interface INodeState {
  id: string;
  cards: ICard[];
  handles: IHandles[];
}

interface ICard {
  id: string;
  type: string;
  title: string;
  buttons: IButtons[];
}

interface IButtons {
  title: string;
  id: string;
}

interface IHandles {
  type: string;
  position: string;
  id: string;
  style: object;
}

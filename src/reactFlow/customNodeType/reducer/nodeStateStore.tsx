import { useState, useEffect, useMemo } from "react";
import { createContainer } from "unstated-next";
import { cloneDeep } from "lodash";
import { Position, useUpdateNodeInternals } from "react-flow-renderer";
import { INodeState } from "./interface";
import { findIndex } from "lodash";
import arrayMove from "array-move";
import { FlowGraphState } from "../../../reducer/flowGraphStore";
const initialNodeState: INodeState = {
  id: "shouldHaveUniqueId",
  cards: [
    {
      id: "shouldHaveUniqueId",
      type: "textWithButton",
      title: "",
      buttons: [{ title: "Samuel", id: "shouldHaveUniqueId" }],
    },
  ],
  handles: [],
};

function useNodeState(initialState = initialNodeState) {
  const [nodeState, setNodeState] = useState(initialState);
  const [containerPosition, setContainerPosition] = useState<any>({});
  const [handlesPosition, setHandlesPosition] = useState<Array<Object | null>>([]);
  const updateNodeInternals = useUpdateNodeInternals();
  const { editNode,deleteEdgeByHandleId, setNodeDraggable } = FlowGraphState.useContainer();

  useEffect(() => {
    updateNodeInternals(nodeState.id);
    editNode(nodeState.id,nodeState)
  }, [nodeState]);

  const handles = useMemo(() => {
    console.log("handlesPosition", handlesPosition);
    return handlesPosition.map((handlePosition: any) => {
      const calculatedPosition =
        ((handlePosition.current.offsetTop + handlePosition.current.clientHeight / 2) /
          containerPosition.current.offsetHeight) *
        100;
      return {
        type: "source",
        position: Position.Right,
        id: handlePosition.id,
        style: { top: `${calculatedPosition}%` },
      };
    });
  }, [containerPosition, handlesPosition]);

  useEffect(() => {
    setNodeState((prevState) => ({ ...prevState, handles: handles }));
  }, [handles]);

  const addCard = () => {
    const temp = cloneDeep(nodeState.cards);
    temp.push({ id: Math.random().toString(36).substr(2, 5), type: "textWithButton", title: "", buttons: [] });
    setNodeState((prev: any) => ({ ...prev, cards: temp }));
  };

  const editTWBTitle = (title: string, index: number) => {
    const temp = cloneDeep(nodeState.cards);
    temp[index].title = title;
    setNodeState((prev: any) => ({ ...prev, cards: temp }));
  };

  const addTWBButton = (index: number) => {
    const temp = cloneDeep(nodeState.cards);
    temp[index].buttons.push({ title: "", id: Math.random().toString(36).substr(2, 4) });
    setNodeState((prev: any) => ({ ...prev, cards: temp }));
  };

  const editTWBButton = (title: string, cardIndex: number, buttonIndex: number) => {
    const temp = cloneDeep(nodeState.cards);
    temp[cardIndex].buttons[buttonIndex].title = title;
    setNodeState((prev: any) => ({ ...prev, cards: temp }));
  };
  const deleteTWBButton = (cardIndex: any, buttonIndex: any, handleId: any) => {
    deleteHandlesPosition(handleId);
    const temp: any = cloneDeep(nodeState.cards);
    temp[cardIndex].buttons.splice(buttonIndex, 1);
    setNodeState((prev: any) => ({ ...prev, cards: temp }));
  };
  const saveNodePosition = (nodeRefPosition: any) => {
    setContainerPosition(nodeRefPosition);
  };

  const saveHandlesPosition = (handleRefPosition: any) => {
    console.log("saveHandlesPosition");
    const temp = cloneDeep(handlesPosition);
    temp.push(handleRefPosition);
    setHandlesPosition(temp);
  };

  const deleteHandlesPosition = (handleId: string) => {
    const temp = cloneDeep(handlesPosition);
    const foundExistedHandleIndex = findIndex(temp, ["id", handleId]);
    if (foundExistedHandleIndex >= 0) {
      temp.splice(foundExistedHandleIndex, 1);
      setHandlesPosition(temp);
    }
    deleteEdgeByHandleId(handleId);
  };

  const onSortEnd = ({ oldIndex, newIndex }: any) => {
    const temp: any = cloneDeep(nodeState.cards);
    const movedArray = arrayMove(temp, oldIndex, newIndex);
    setNodeState((prev: any) => ({ ...prev, cards: movedArray }));
  };

  const onSortSetNodeDraggable = (draggable: boolean) => {
    setNodeDraggable(nodeState.id, draggable);
  };

  return {
    nodeState,
    addCard,
    editTWBTitle,
    addTWBButton,
    editTWBButton,
    deleteTWBButton,
    saveNodePosition,
    saveHandlesPosition,
    deleteHandlesPosition,
    handlesPosition,
    onSortEnd,
    onSortSetNodeDraggable,
  };
}

export const NodeState = createContainer(useNodeState);

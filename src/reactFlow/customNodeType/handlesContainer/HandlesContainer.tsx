import React, { useEffect } from "react";
import { NodeState } from "../../customNodeType/reducer/nodeStateStore";
import { Handle } from "react-flow-renderer";

interface Props {}

const HandlesContainer = (props: Props) => {
  const { nodeState } = NodeState.useContainer();
  const { handles } = nodeState;

  return (
    <React.Fragment>
      {handles.map((handle: any) => (
        <Handle type={handle.type} key={handle.id} position={handle.position} style={handle.style} id={handle.id} />
      ))}
    </React.Fragment>
  );
};

export default HandlesContainer;

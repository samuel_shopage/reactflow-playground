import { Handle, Position } from "react-flow-renderer";
import EditableCardContainer from "../cardContainer/editableCardContainer/EditableCardContainer";
import HandlesContainer from "./handlesContainer/HandlesContainer";
import { NodeState } from "./reducer/nodeStateStore";
import s from "./CustomNode.module.scss";
import NodeStateProvider from "./reducer/NodeStateProvider";

const CustomNode = ({ id, data }: any) => {
  return (
    <NodeStateProvider id={id} data={data}>
      <div className={s.customNode}>
        <EditableCardContainer />
        <Handle type="target" position={Position.Top} id={"target"} />
        <HandlesContainer />
        <Handle type="source" position={Position.Bottom} id={"source"} />
      </div>
    </NodeStateProvider>
  );
};

export default CustomNode;

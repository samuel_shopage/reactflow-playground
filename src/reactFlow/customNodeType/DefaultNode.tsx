import { Handle, Position } from "react-flow-renderer";
import EditableCardContainer from "../cardContainer/editableCardContainer/EditableCardContainer";
import HandlesContainer from "./handlesContainer/HandlesContainer";
import { NodeState } from "./reducer/nodeStateStore";
import s from "./DefaultNode.module.scss";
import NodeStateProvider from "./reducer/NodeStateProvider";

const DefaultNode = ({ id, data }: any) => {
  
  return (
    <NodeStateProvider id={id} data={data}>
      <div className={s.defaultNode}>
        <EditableCardContainer />
        <HandlesContainer />
        <Handle type="source" position={Position.Bottom} id={"source"} />
      </div>
    </NodeStateProvider>
  );
};

export default DefaultNode;

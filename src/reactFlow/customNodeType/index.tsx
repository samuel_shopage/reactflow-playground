import DefaultNode from "./DefaultNode";
import CustomNode from "./CustomNode";
export const nodeType = {
  defaultNode: DefaultNode,
  customNode: CustomNode,
};

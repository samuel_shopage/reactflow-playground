import React from "react";
import { Controls, ControlButton } from "react-flow-renderer";
import { PlusOutlined } from "@ant-design/icons";
import PopoverContainer from "./popoverContainer/PopoverContainer";
interface Props {
  showZoom: boolean;
  showFitView: boolean;
  showInteractive: boolean;
}

const CustomControl = ({ showZoom, showFitView, showInteractive }: Props) => {
  return (
    <Controls showZoom={showZoom} showFitView={showFitView} showInteractive={showInteractive}>
      <PopoverContainer>
        <ControlButton>
          <PlusOutlined />
        </ControlButton>
      </PopoverContainer>
    </Controls>
  );
};

export default CustomControl;

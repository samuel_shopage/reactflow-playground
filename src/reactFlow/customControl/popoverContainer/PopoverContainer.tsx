import { ReactNode } from "react";
import { Popover } from "antd";
import AddNewNode from "../popoverContainer/addNewNode/AddNewNode";
import s from "./PopoverContainer.module.scss";
interface Props {
  content?: ReactNode;
  children: ReactNode;
}

const PopoverContainer = ({ content = AddNewNode, children }: Props) => {
  return (
    <Popover overlayClassName={s.popoverContainer} content={content} trigger="click" placement="rightBottom">
      {children}
    </Popover>
  );
};

export default PopoverContainer;

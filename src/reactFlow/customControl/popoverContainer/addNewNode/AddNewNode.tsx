import React from "react";
import s from "./AddNewNode.module.scss";
import { FontColorsOutlined, CommentOutlined } from "@ant-design/icons";
import { FlowGraphState } from "../../../../reducer/flowGraphStore";
interface Props {}

const AddNewNode = (props: Props) => {
  const { addNode } = FlowGraphState.useContainer();

  return (
    <div className={s.addNewNode}>
      <div onClick={addNode}>
        <FontColorsOutlined />
        <div>Text + Button</div>
      </div>
      <div>
        <CommentOutlined />
        <div>Send Message</div>
      </div>
    </div>
  );
};

export default AddNewNode;

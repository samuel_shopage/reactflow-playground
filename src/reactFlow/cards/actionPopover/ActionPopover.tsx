import React from "react";
import { ReactNode } from "react";
import { Popover, Button } from "antd";
import s from "./ActionPopover.module.scss";
interface Props {
  children: ReactNode;
  actions: any[];
}

const ActionPopover = ({ children, actions }: Props) => {
  const content = (
    <div className={s.content}>
      {actions.map((action, index) => (
        <div key={index} onClick={action.onClick}>
          {action.title}
        </div>
      ))}
    </div>
  );
  return (
    <Popover overlayClassName={s.actionPopover} placement="left" content={content}>
      <div>{children}</div>
    </Popover>
  );
};

export default ActionPopover;

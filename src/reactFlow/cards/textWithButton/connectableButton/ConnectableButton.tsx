import React, { useState, useEffect, useRef } from "react";
import { Input, Button } from "antd";
import { NodeState } from "../../../customNodeType/reducer/nodeStateStore";
import { isEmpty } from "lodash";
const ConnectableButton = ({ title, id, cardIndex, buttonIndex }: any) => {
  const { editTWBButton, saveHandlesPosition } = NodeState.useContainer();
  const [handleRef, setHandleRef] = useState<HTMLDivElement | null>(null);
  useEffect(() => {
    if (!isEmpty(handleRef)) {
      saveHandlesPosition({
        id: id,
        current: handleRef,
      });
    }
  }, [handleRef]);

  return (
    <div ref={(ref) => setHandleRef(ref)}>
      <Input
        id={id}
        placeholder={"Add button name"}
        value={title}
        onChange={(e) => editTWBButton(e.target.value, cardIndex, buttonIndex)}
      />
    </div>
  );
};

export default ConnectableButton;

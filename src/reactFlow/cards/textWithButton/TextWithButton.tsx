import React, { useState, useEffect } from "react";
import { Input, Button } from "antd";
import style from "../cards.module.scss";
import ConnectableButton from "./connectableButton/ConnectableButton";
import { NodeState } from "../../customNodeType/reducer/nodeStateStore";
import { Popover } from "antd";
import ActionPopover from "../actionPopover/ActionPopover";
const TextWithButton = ({ card, cardIndex }: any) => {
  const { editTWBTitle, addTWBButton, deleteTWBButton, deleteHandlesPosition } = NodeState.useContainer();
  const { title, buttons } = card;

  const deleteButton = (cardIndex: any, buttonIndex: any, id: any) => {
    deleteTWBButton(cardIndex, buttonIndex, id);
  };
  return (
    <div className={style.card}>
      <Input
        placeholder={"Add text"}
        value={title}
        onChange={(e) => {
          editTWBTitle(e.target.value, cardIndex);
        }}
      />
      {buttons.map((button: any, index: number) => (
        <div key={button.id}>
          <ActionPopover actions={[{ title: "Delete", onClick: () => deleteButton(cardIndex, index, button.id) }]}>
            <ConnectableButton title={button.title} id={button.id} cardIndex={cardIndex} buttonIndex={index} />
          </ActionPopover>
        </div>
      ))}
      <Button className={style.button} onClick={() => addTWBButton(cardIndex)}>
        {"+ Add Button"}
      </Button>
    </div>
  );
};

export default TextWithButton;

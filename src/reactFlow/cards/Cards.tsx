import React from "react";
import TextWithButton from "./textWithButton/TextWithButton";
const Cards = ({ card, type, cardIndex }: any) => {
  if (type === "textWithButton") {
    return <TextWithButton card={card} cardIndex={cardIndex} />;
  }
  return <></>;
};

export default Cards;

import { ReactFlowProvider, Position } from "react-flow-renderer";
import FlowGraph from "./FlowGraph";
import { FlowGraphState } from "../reducer/flowGraphStore";
interface Props {}

const initialFlowGraphState = [
  {
    id: Math.random().toString(36).substr(2, 6),
    type: "defaultNode",
    position: { x: 100, y: 200 },
    data: {
      cards: [
        {
          id: Math.random().toString(36).substr(2, 5),
          type: "textWithButton",
          title: "",
          buttons: [{ title: "Samuel", id: Math.random().toString(36).substr(2, 4) }],
        },
      ],
      handles: [
        {
          type: "source",
          position: Position.Right,
          id: Math.random().toString(36).substr(2, 4),
          style: { top: "43%" },
        },
        {
          type: "source",
          position: Position.Right,
          id: Math.random().toString(36).substr(2, 4),
          style: { top: "57.8%" },
        },
      ],
    },
    draggable: true,
  },
  {
    id: Math.random().toString(36).substr(2, 6),
    data: { label: "2" },
    position: { x: 450, y: 300 },
    draggable: true,
  },
  {
    id: Math.random().toString(36).substr(2, 6),
    data: { label: "Node 3" },
    position: { x: 450, y: 500 },
    draggable: true,
  },
  {
    id: Math.random().toString(36).substr(2, 6),
    type: "customNode",
    position: { x: 800, y: 200 },
    data: {
      cards: [
        {
          id: Math.random().toString(36).substr(2, 5),
          type: "textWithButton",
          title: "",
          buttons: [{ title: "Samuel", id: Math.random().toString(36).substr(2, 4) }],
        },
      ],
      handles: [
        {
          type: "source",
          position: Position.Right,
          id: Math.random().toString(36).substr(2, 4),
          style: { top: "43%" },
        },
        {
          type: "source",
          position: Position.Right,
          id: Math.random().toString(36).substr(2, 4),
          style: { top: "57.8%" },
        },
      ],
    },
    draggable: true,
  },
];

const RFProvider = (props: Props) => {
  return (
    <div>
      <ReactFlowProvider>
        <FlowGraphState.Provider initialState={initialFlowGraphState}>
          <FlowGraph />
        </FlowGraphState.Provider>
      </ReactFlowProvider>
    </div>
  );
};

export default RFProvider;

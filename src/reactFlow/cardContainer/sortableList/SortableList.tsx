import { SortableContainer, SortableElement } from "react-sortable-hoc";
import Cards from "../../cards/Cards";
import s from "./SortableList.module.scss";
import { SortableHandle } from "react-sortable-hoc";
const DragHandle = SortableHandle(() => <span>::</span>);

const SortableItem = SortableElement(({ card, type, cardIndex }: any) => {
  return (
    <div draggable onDragStart={() => console.log("dragStart")}>
      <Cards type={type} card={card} cardIndex={cardIndex} />
    </div>
  );
});

const SortableList = SortableContainer(({ cards }: any) => {
  return (
    <div className={s.SortableList}>
      {cards.map((card: any, index: number) => {
        return <SortableItem card={card} type={card.type} index={index} key={card.id} cardIndex={index} />;
      })}
    </div>
  );
});

export default SortableList;

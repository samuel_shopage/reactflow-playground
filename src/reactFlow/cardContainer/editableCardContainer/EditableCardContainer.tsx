import { useState, useEffect, useMemo } from "react";
import { Button } from "antd";
import s from "./EditableCardContainer.module.scss";
import { NodeState } from "../../customNodeType/reducer/nodeStateStore";
import SortableList from "../sortableList/SortableList";
import { useStore } from "react-flow-renderer";
const EditableCardContainer = ({}: any) => {
  const [nodeRef, setNodeRef] = useState<HTMLDivElement | null>(null);
  const store = useStore();
  const { transform } = store.getState();
  const { nodeState, onSortEnd, addCard, saveNodePosition, onSortSetNodeDraggable } = NodeState.useContainer();
  const { cards } = nodeState;
  const [helperContainer, setHelperContainer] = useState<any>();

  useEffect(() => {
    if (nodeRef) {
      saveNodePosition({
        id: nodeState.id,
        current: nodeRef,
      });
    }
  }, [nodeState.cards, nodeRef]);

  const topPadding = useMemo(() => {
    if (helperContainer && nodeRef) {
      const ratio = transform[2];
      const helperContainerPos = helperContainer?.getBoundingClientRect();
      const nodeRefPos = nodeRef?.getBoundingClientRect();
      return helperContainerPos.top / ratio - nodeRefPos!.top / ratio;
    }
  }, [helperContainer, nodeState, transform]);

  const ghostDiv = document.getElementById(`sortableGhost_${nodeState.id}`);
  return (
    <div ref={(ref) => setNodeRef(ref)}>
      <div id={`sortableGhost_${nodeState.id}`} style={{ top: `${topPadding}px` }} className={s.sortableGhost} />
      <div className={s.editableCardContainer}>
        <SortableList
          cards={cards}
          lockAxis="y"
          onSortEnd={({ oldIndex, newIndex }: any) => {
            onSortEnd({ oldIndex, newIndex });
            onSortSetNodeDraggable(true);
          }}
          helperContainer={ghostDiv}
          helperClass={s.sortingHelperClass}
          onSortStart={({ node }: any) => {
            setHelperContainer(node);
            onSortSetNodeDraggable(false);
          }}
          // useDragHandle
        />
        <div>
          <Button className={s.button} onClick={addCard}>
            {"+ Add Cards"}
          </Button>
        </div>
      </div>
    </div>
  );
};

export default EditableCardContainer;

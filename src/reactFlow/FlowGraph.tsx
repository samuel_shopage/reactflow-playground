import React, { useState, useEffect } from "react";
import s from "./FlowGraph.module.scss";
import ReactFlow, {
  Background,
  addEdge,
  removeElements,
  Edge,
  Connection,
  Elements,
  Position,
  useZoomPanHelper,
  useStore,
  isNode,
  Node,
  useStoreState,
  Controls,
} from "react-flow-renderer";
import CustomControl from "./customControl/CustomControl";
import { nodeType } from "./customNodeType/index";
import { FlowGraphState } from "../reducer/flowGraphStore";

interface Props {}

const FlowGraph = (props: Props) => {
  const { flowGraphState, setFlowGraphState } = FlowGraphState.useContainer();
  const onElementsRemove = (elementsToRemove: any) => setFlowGraphState((els) => removeElements(elementsToRemove, els));
  const onConnect = (params: any) => setFlowGraphState((els) => addEdge(params, els));
  // const store = useStore();
  // const { selectedElements } = store.getState();
  // useEffect(() => {
  //   console.log("selectedElements", selectedElements);
  // }, [selectedElements]);

  return (
    <div className={s.flowGraph}>
      <ReactFlow
        className={s.reactFlow}
        elements={flowGraphState}
        nodeTypes={nodeType}
        onElementsRemove={onElementsRemove}
        onConnect={onConnect}
        nodesDraggable={true}
      >
        <Background className={s.background} color={"#f3f3f3"} />
        <CustomControl showZoom={false} showFitView={true} showInteractive={true} />
      </ReactFlow>
    </div>
  );
};

export default FlowGraph;

import { useState, useEffect, useMemo } from "react";
import { createContainer } from "unstated-next";
import { cloneDeep } from "lodash";
import { Position, Edge } from "react-flow-renderer";
import { IFlowGraphState } from "./interface";
const initialFlowGraphState = [
  {
    id: "1",
    data: { label: "1" },
    position: { x: 100, y: 200 },
  },
  {
    id: "2",
    data: { label: "2" },
    position: { x: 450, y: 300 },
  },
  { id: "3", data: { label: "3" }, position: { x: 450, y: 500 } },
  {
    id: "4",
    data: { label: "4" },
    position: { x: 800, y: 200 },
  },
];

function useFlowGraphState(initialState: Array<IFlowGraphState | Edge> = initialFlowGraphState) {
  const [flowGraphState, setFlowGraphState] = useState(initialState);
  useEffect(() => {
    console.log(flowGraphState);
  }, [flowGraphState]);

  const addNode = () => {
    const newNode = {
      id: Math.random().toString(36).substr(2, 6),
      type: "customNode",
      position: {
        x: 500,
        y: 500,
      },
      data: {
        cards: [],
        handles: [],
      },
    };
    setFlowGraphState((els) => els.concat(newNode));
  };
  
  const editNode = (nodeId:string,nodeState:any) =>{
    const {cards,handles} = nodeState
    const temp = cloneDeep(flowGraphState);
    temp[temp.findIndex((el)=>el.id===nodeId)].data={cards,handles};
    setFlowGraphState(temp);
    }

  const deleteEdgeByHandleId = (handleId: any) => {
    const temp = flowGraphState.filter((element: any) => element.sourceHandle !== handleId);
    setFlowGraphState(temp);
  };

  const setNodeDraggable = (nodeId: string, draggable: boolean) => {
    // const temp = flowGraphState.find((element: any) => element.id === nodeId) as IFlowGraphState;
    // temp.draggable = draggable;
    const temp = flowGraphState.map((element: any) => {
      if (element.id === nodeId) {
        element.draggable = draggable;
      }
      return element;
    });
    console.log("temp", temp);
    setFlowGraphState(temp);
  };

  return {
    flowGraphState,
    setFlowGraphState,
    addNode,
    editNode,
    deleteEdgeByHandleId,
    setNodeDraggable,
  };
}

export const FlowGraphState = createContainer(useFlowGraphState);

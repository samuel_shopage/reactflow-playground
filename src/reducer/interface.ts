export interface IFlowGraphState {
  id: string;
  type?: string;
  position: IPosition;
  data?: IData | any;
  draggable?: boolean;
}

interface IPosition {
  x: number;
  y: number;
}

interface IData {
  cards: Array<ITextWithButtonCard>;
  handles: IHandles[];
}

interface ITextWithButtonCard {
  id: string;
  type: string;
  title: string;
  buttons: IButton[];
}

interface IButton {
  title: string;
  id: string;
}

interface IHandles {
  type: string;
  position: string;
  id: string;
  style: object;
}

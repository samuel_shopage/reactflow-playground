import React from "react";
import RFProvider from "./reactFlow/RFProvider";
function App() {
  return <RFProvider />;
}

export default App;
